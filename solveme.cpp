#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <map>
#include <memory>
#include <string>
#include <algorithm>

#include <sstream>
#include <iterator>
#include <numeric>
#include <chrono>

using std::endl;
using std::cout;
using std::vector;
using std::string;
using std::begin;
using std::end;
using std::map;


class Feature
{
   float x, y;
   std::string name;
   public:
      Feature(std::string _name, float _x, float _y): x(_x),y(_y),name(_name) {}
      Feature(Feature const &f) = default;
      Feature& operator=(Feature const &f) = default;
      void printMe()
      {
         cout << "Feature: " << x << ", " << y << ", " << name << endl;
      }
      string const & getName() const 
      {
         return name;
      }
      float dst2(Feature& f) const
      {
         return pow(x - f.x, 2) + pow(y - f.y, 2);
      }
      float dst2(Feature* f) const
      {
         if (!f) return FLT_MAX;
         return pow(x - f->x, 2) + pow(y - f->y, 2);
      }
      float operator[](size_t ind) const
      {
         if (ind == 0) return x;
         return y;
      }
      bool operator==(Feature &f) const
      {
         if (name == f.name) return true;
         return false;
      }
      ~Feature()
      {
         //cout << "destroying: " << name << endl;
      }

};

template <class T>
class KDtreeLeaf
{
public:
   T pt;
   KDtreeLeaf *l;
   KDtreeLeaf *r;

   KDtreeLeaf(T _pt) :l(nullptr), r(nullptr), pt(_pt) {};
   ~KDtreeLeaf();
};

template <class T> KDtreeLeaf<T>::~KDtreeLeaf()
{
   delete l;
   delete r;
   //cout << "destroying KDtreeLeaf" << endl;
}

typedef KDtreeLeaf<Feature> KDtreeFLeaf;

KDtreeFLeaf* buildKDTree(vector<Feature>& fts, vector<Feature>::iterator bg, vector<Feature>::iterator ed, size_t depth =0)
{
   size_t size = std::distance(bg,ed);
   KDtreeFLeaf* pt = nullptr;

   if (size == 0) return nullptr;
   if (size == 1) return new KDtreeFLeaf(*bg);
   size_t split = size / 2;

   //std::sort(bg, ed, [depth](auto const &a, auto const &b) {return a[depth % 2] < b[depth % 2]; });
   std::nth_element(bg, bg+split, ed, [depth](auto const &a, auto const &b) {return a[depth % 2] < b[depth % 2]; });
   
   pt = new KDtreeFLeaf(*(bg+split));
   depth++;
   if (size == 2)
   {
      pt->l = new KDtreeFLeaf(*(bg));
   }
   else if (size == 3)
   {
      pt->l = new KDtreeFLeaf(*(bg));
      pt->r = new KDtreeFLeaf(*(bg+2));
   }
   else
   {
      pt->l = buildKDTree(fts, bg, bg + split, depth);
      pt->r = buildKDTree(fts, bg + split + 1, ed, depth);
   }
   return pt;
}

Feature* closerDst(Feature *p, Feature *p1, Feature *p2)
{
   if (!p1) return p2;
   if (!p2) return p1;
   if (p->dst2(p1) < p->dst2(p2)) return p1;
   return p2;
}

Feature* nearestByKd(KDtreeFLeaf* root, Feature& pt, size_t depth=0)
{
   if (!root) { return nullptr; }
   size_t axis = depth % 2;
   KDtreeFLeaf* nextBr, *oppositeBr;
   Feature* best = nullptr;
   if (pt[axis] < root->pt[axis])
   {
      nextBr = root->l;
      oppositeBr = root->r;
   }
   else
   {
      nextBr = root->r;
      oppositeBr = root->l;
   }

   best = closerDst(&pt, nearestByKd(nextBr, pt, depth + 1), &(root->pt));
   if (pt.dst2(best) > pow(root->pt[axis] - pt[axis],2))
   {
      best = closerDst(&pt, nearestByKd(oppositeBr, pt, depth + 1), best);
   }
   return best;
}

Feature* nearestByKdSkipSelf(KDtreeFLeaf* root, Feature& pt, size_t depth = 0)
{
   if (!root) { return nullptr; }
   size_t axis = depth % 2;
   KDtreeFLeaf* nextBr, *oppositeBr;
   Feature* best = nullptr;

   if (pt[axis] < root->pt[axis])
   {
      nextBr = root->l;
      oppositeBr = root->r;
   }
   else
   {
      nextBr = root->r;
      oppositeBr = root->l;
   }

   if (pt == root->pt)
   {
      best = closerDst(&pt, nearestByKdSkipSelf(nextBr, pt, depth + 1),
                            nearestByKdSkipSelf(oppositeBr, pt, depth + 1));
   }
   else
   {
      best = closerDst(&pt, nearestByKdSkipSelf(nextBr, pt, depth + 1), &(root->pt));
      if (pt.dst2(best) > pow(root->pt[axis] - pt[axis], 2))
      {
         best = closerDst(&pt, nearestByKdSkipSelf(oppositeBr, pt, depth + 1), best);
      }
   }
   return best;
}

struct membuf : std::streambuf
{
   membuf(char* begin, char* end) {
      this->setg(begin, begin, end);
   }
};

vector<Feature> loadFeatures(std::ifstream& f)
{
   f.seekg(0, f.end);
   size_t bytes = f.tellg();
   f.seekg(0);

   char *buf = new char[bytes];
   f.read((char*)buf, bytes);

   size_t lineCount = std::count(buf, buf+bytes,'\n');
   auto v = std::vector<Feature>();
   v.reserve(lineCount);
   
   string line;
   f.clear();
   f.seekg(0);
   
   //nice soluions were 1s slower on load.
   membuf  mb(buf, buf + bytes);
   std::istream ss(&mb);
   size_t stop = 0;
   while (stop < (bytes-1) && std::getline(ss, line))
   {
      stop += line.size()+2;
      size_t br = line.find(' ');
      size_t br2 = line.find(' ',br+1);
      v.emplace_back( line.substr(0, br), 
                      std::stof(line.substr(br + 1, br2 - br - 1)),
                      std::stof(line.substr(br2 + 1, br2 - line.length()))
                     );
   }
   delete[] buf;
   return v;
}

int main(int argc, char *argv[])
{
   if (argc < 2) 
   {
      cout << "usage: " << argv[0] << " inputfile" << endl;
      return 1;
   }
   std::ifstream f(argv[1]);
   if (f.is_open())
   {
      try
      {
         using std::chrono::system_clock;
         using std::chrono::time_point;
         using std::chrono::duration;

         auto t0 = system_clock::now();
         auto features = loadFeatures(f);

         auto tLoaded = system_clock::now();
         //cout << "loaded..."<< size(features) << endl ;
         KDtreeFLeaf* top = buildKDTree(features, begin(features), end(features));
         auto tBuildt = system_clock::now();

         map<string, float> results;
         for (auto ft: features)
         {
            Feature *fn = nearestByKdSkipSelf(top, ft);
            results.emplace(ft.getName(), ft.dst2(fn));
         }
         auto tKDSearched = system_clock::now();

         auto res = *std::max_element(begin(results), end(results), [](const auto & p1, const auto & p2) { return p1.second < p2.second;});

         auto tSearched = system_clock::now();
         //cout << std::fixed << res.first << " " << res.second;
         cout << res.first << endl;
         if (argc > 2)
         {
            cout << "-----------------------------" << endl;
            cout << "load: " << (duration<double>(tLoaded - t0)).count() << "s" << endl;
            cout << "building KD: " << (duration<double>(tBuildt - tLoaded)).count() << "s" << endl;
            cout << "searches: " << (duration<double>(tKDSearched - tBuildt)).count() << "s" << endl;
            cout << "find max: " << (duration<double>(tSearched - tKDSearched)).count() << "s" << endl;
            cout << "total: " << (duration<double>(tSearched - t0)).count() << "s" << endl;
         }
         delete top;

      }
      catch (const std::exception& e)
      {
         cout << "problems... " << e.what();
         return 1;
      }
      catch (...)
      {
         cout << "problems...." << endl;
         return 1;
      }
   }
   else
   {
      cout << "problems oppening " << argv[1] << endl;
      return 1;
   }
   return 0;
}