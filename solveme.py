#! /usr/bin/env python3
import sys

def load_features(fn):
   with open(fn,'r') as f:
      features = [(lambda x: [int(x[1]),int(x[2]),x[0]])(line.split(' ')) for line in f.readlines()]
   return features

def dst2(a,b):
   return (a[0]-b[0])**2+(a[1]-b[1])**2

def build_kd(points, depth=0):
   n = len(points)
   if (n<1): return None

   sorted_points = sorted(points,key = lambda x:x[depth%2])
   split = n//2
   return {
      'point': sorted_points[split],
      'left':  build_kd(sorted_points[:split], depth+1),
      'right': build_kd(sorted_points[split+1:], depth+1),
   }

def closer_distance(pivot, p1, p2):
   if p1 is None: return p2
   if p2 is None: return p1

   d1,d2 = dst2(pivot, p1), dst2(pivot, p2)

   if d1<d2: return p1
   else: return p2

def nearest_by_kd(root, point, depth=0):
   if root is None:
      return None
   axis = depth%2
   next_branch, opposite_branch = (root['left'], root['right']) if point[axis] < root['point'][axis] else (root['right'], root['left'])

   best = closer_distance(point, nearest_by_kd(next_branch, point, depth+1), root['point'])

   if dst2(best, point) > (root['point'][axis]-point[axis])**2:
      best = closer_distance(point, nearest_by_kd(opposite_branch, point, depth+1), best)

   return best

def nearest_by_kd_skip_self(root, point, depth=0):
   if root is None:
      return None
   axis = depth%2
   depth+=1

   if point[axis] < root['point'][axis]:
      next_branch, opposite_branch = root['left'], root['right']
   else:
      next_branch, opposite_branch = root['right'], root['left']

   if root['point'][2] == point[2]:
      best = closer_distance(point, nearest_by_kd_skip_self(next_branch, point, depth), 
                                    nearest_by_kd_skip_self(opposite_branch, point, depth))
   else:
      best = closer_distance(point, nearest_by_kd_skip_self(next_branch, point, depth), root['point'])
      if dst2(best, point) >= ( root['point'][axis]-point[axis])**2:
         best = closer_distance(point, nearest_by_kd_skip_self(opposite_branch, point, depth), best)

   return best

if __name__ == '__main__':
   try:
      features = load_features(sys.argv[1])
      # print('loaded %d features.'%len(features))
      kdtree = build_kd(features)
      # print("built.")

      for ft in features:
         nearest = nearest_by_kd_skip_self(kdtree, ft)
         ft.append(dst2(ft, nearest))
         ft.append(nearest[2])

      most_remote=features[0]
      for ft in features[1:]:
         if most_remote[3]<ft[3]:
            most_remote = ft

      print(most_remote[2])

   except KeyError:
      print("Problem loading features from file. use: %s featurefile"%sys.argv[0])

